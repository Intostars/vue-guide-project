#### 项目名称：vue-guide-project

#### 项目简介：
- 从零搭建vue3+ts+husky等配置项目
- 用于创建工程化与规范化项目的范例

#### 项目配置： 
- 使用 Prettier 统一格式化代码，集成 ESLint、Stylelint 代码校验规范
- 使用 husky、lint-staged、commitlint、czg、cz-git 规范提交信息
- 内含router、pinia、scss、unocss等配置
- 使用 pinia-plugin-persistedstate 实现pinia持久化
- 集成了SVG、图片压缩、rem、自动重启、gzip压缩、打包分析等plugin
- 集成了.env配置环境变量与情景配置

#### 文档地址
- [01-prettier+stylelint+husky+Lint-staged+Commitlint配置](./docs/01-prettier+stylelint+husky+Lint-staged+Commitlint配置.md)
- [02-router+pinia+scss+unocss+跨域配置](./docs/02-router+pinia+scss+unocss+跨域配置.md)
- [03-图片压缩+rem+自动重启等plugin使用与打包配置](./docs/03-图片压缩+rem+自动重启等plugin使用与打包配置.md)
- [04-.env配置环境变量与情景配置](./docs/04-.env配置环境变量与情景配置.md)