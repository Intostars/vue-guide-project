// rem转换
import { px2rem } from 'vite-plugin-px2rem'

export const Px2remPlugin = () =>
  px2rem({
    width: 750,
    rootFontSize: 16
  })
