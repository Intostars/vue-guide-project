// svg配置
import svgLoader from 'vite-svg-loader'

export const svgLoaderPlugin = () =>
  svgLoader({
    defaultImport: 'url' // or 'raw'
  })
