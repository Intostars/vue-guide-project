import UnoCSS from 'unocss/vite'

export const UnocssPlugin = () => UnoCSS()
