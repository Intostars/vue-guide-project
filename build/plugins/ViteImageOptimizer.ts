// 压缩图片
import { ViteImageOptimizer } from 'vite-plugin-image-optimizer'

export const ViteImageOptimizerPlugin = () =>
  ViteImageOptimizer({
    /* pass your config */
  })
