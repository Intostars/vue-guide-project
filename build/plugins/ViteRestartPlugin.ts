// 自动重启
import ViteRestart from 'vite-plugin-restart'

export const ViteRestartPlugin = () =>
  ViteRestart({
    restart: ['*.config.[jt]s', '**/config/*.[jt]s', '*.config.cjs']
  })
