// 插件配置 总入库
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import { PluginOption } from 'vite'
import VueDevTools from 'vite-plugin-vue-devtools'

import { Px2remPlugin } from './Px2rem' // rem
import { svgLoaderPlugin } from './SvgLoader' // SVG
import { UnocssPlugin } from './Unocss' // unocss
import { VisualizerPlugin } from './Visualizer' // 打包分析
import { ViteCompressionPlugin } from './ViteCompression' // 压缩gzip
import { ViteImageOptimizerPlugin } from './ViteImageOptimizer' // 图片压缩
import { ViteRestartPlugin } from './ViteRestartPlugin' // 修改配置文件自动重启

export const usePlugins = (isBuild: boolean, viteEnv: ViteEnv) => {
  const { VITE_OPEN_VISUALIZER, VITE_OPEN_SVG_LOADER, VITE_OPEN_PX2REM, VITE_OPEN_COMPRESSION } =
    viteEnv
  const plugins: PluginOption[] = [vue(), vueJsx()]

  plugins.push(UnocssPlugin())
  if (VITE_OPEN_PX2REM) plugins.push(Px2remPlugin())
  if (VITE_OPEN_SVG_LOADER) plugins.push(svgLoaderPlugin())

  // 开发模式下
  if (!isBuild) {
    plugins.push(VueDevTools())
    plugins.push(ViteRestartPlugin())
  }

  if (isBuild) {
    plugins.push(ViteImageOptimizerPlugin())
    // 压缩gzip
    VITE_OPEN_COMPRESSION && plugins.push(ViteCompressionPlugin())
    // 打包分析
    VITE_OPEN_VISUALIZER && plugins.push(VisualizerPlugin())
  }
  return plugins
}
