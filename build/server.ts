import type { ProxyOptions } from 'vite'

type ProxyItem = [string, string]
type ProxyList = ProxyItem[]
type ProxyTargetList = Record<string, ProxyOptions>

/**
 * 创建代理，用于解析 .env.development 代理配置
 */
const userProxy = (proxyList: ProxyList = []) => {
  const ret: ProxyTargetList = {}

  proxyList.forEach((item) => {
    const [prefix, target] = item
    const httpsRE = /^https:\/\//
    const isHttps = httpsRE.test(target)
    ret[prefix] = {
      target,
      changeOrigin: true,
      ws: true,
      rewrite: (path: string) => path.replace(new RegExp(`^${prefix}`), ''),
      // https is require secure=false
      // Verify SSL certificate
      ...(isHttps ? { secure: false } : {})
    }
  })

  return ret
}

/**
 * server 配置
 * @returns
 */
export const useServer = (viteEnv: ViteEnv) => {
  const { VITE_PORT, VITE_PROXY } = viteEnv
  return {
    // 监听所有公共ip
    // host: '0.0.0.0',
    cors: true,
    port: VITE_PORT,
    proxy: userProxy(VITE_PROXY)
  }
}
