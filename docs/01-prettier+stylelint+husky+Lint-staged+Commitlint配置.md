# 从零构建 vue3+ts+husky 等基础项目

## 0、vue3 基础插件

![vue_official](imgs/vue_official.png)

![vue3_snippets](imgs/vue3_snippets.png)

## 一、 安装 vue3 脚手架 [地址](https://cn.vuejs.org/guide/quick-start.html)

### 1. 推荐官方脚手架 使用官网推荐的新版本 Eslint&prettier 配置

```js
# 根据具体情况可以选择npm yarn pnpm
# 下面选择pnpm为例
pnpm create vue@latest

# 选择根据提示选择
Vue.js - The Progressive JavaScript Framework

√ 请输入项目名称： ... vue-project
√ 是否使用 TypeScript 语法？ ... 否 / #是
√ 是否启用 JSX 支持？ ... 否 / #是
√ 是否引入 Vue Router 进行单页面应用开发？ ... 否 / #是
√ 是否引入 Pinia 用于状态管理？ ... 否 / #是
√ 是否引入 Vitest 用于单元测试？ ... #否 / 是
√ 是否要引入一款端到端（End to End）测试工具？ » #不需要
√ 是否引入 ESLint 用于代码质量检测？ ... 否 / #是
√ 是否引入 Prettier 用于代码格式化？ ... 否 / #是
```

![vue_project](imgs/vue_project.png)

### 2. 安装依赖&启动项目

```js
# 进入项目
cd vue-project
# 安装依赖包
pnpm i
# 格式化项目(可选)
pnpm format
# 启动项目
pnpm run dev
```

## 二、 Eslint & prettier & airbnb 规范 配置

### 1. 安装 vscode 的 Eslint 和 prettier 插件

![image-20240207095535232](imgs/image-20240207095535232.png)

![image-20240207095552245](imgs/image-20240207095552245.png)

> eslint https://eslint.nodejs.cn/docs/latest/use/getting-started 代码质量检查工具
>
> prettier https://www.prettier.cn/docs/index.html 代码风格格式化工具

### 2. 安装依赖包

```js
/*
eslint-config-airbnb-base airbnb 规范
eslint-import-resolver-typescript 解决@别名识别问题
eslint-plugin-import 如果是npm yarn需要安装
*/
pnpm i eslint-config-airbnb-base eslint-plugin-import eslint-import-resolver-typescript  -D
```

> eslint-config-airbnb-base 
>[**airbnb 代码质量规范**](https://github.com/airbnb/javascript#events)
>[Airbnb JavaScript 代码规范](https://www.w3cschool.cn/rtuhtw/)
>
> eslint-import-resolver-typescript 
>https://www.npmjs.com/package/eslint-import-resolver-typescript 解决@别名识别问题

### 3. 根目录下新建规则配置文件

![image-20240207100604026](imgs/image-20240207100604026.png)

- `.eslintrc.cjs` 修改规则

  ```js
  /* eslint-env node */
  require("@rushstack/eslint-patch/modern-module-resolution");

  module.exports = {
    root: true,
    extends: [
      "plugin:vue/vue3-essential",
      "eslint:recommended",
      "airbnb-base",
      "@vue/eslint-config-typescript",
      "@vue/eslint-config-prettier/skip-formatting",
      //  './.eslintrc-auto-import.json' 后面自动导入会用到
    ],
    parserOptions: {
      ecmaVersion: "latest",
    },
    overrides: [
      {
        files: ["*.ts", "*.tsx", "*.vue"],
        rules: {
          // 解决 ts 全局类型定义后，eslint报错的问题
          "no-undef": "off",
        },
      },
    ],
    // 解决@别名识别问题
    settings: {
      "import/resolver": {
        typescript: {
          project: "./tsconfig.*.json",
        },
      },
    },
    rules: {
      "import/no-extraneous-dependencies": "off",
      // 对后缀的检测
      "import/extensions": [
        "error",
        "ignorePackages",
        { js: "never", jsx: "never", ts: "never", tsx: "never" },
      ],
      // eslint-plugin-import 控制 import引入排序问题
      "import/order": [
        "error",
        {
          groups: [
            /* 
              builtin ：内置模块，如 path，fs等 Node.js内置模块。
              external ：外部模块，如 lodash ，react 等第三方库。
              internal ：内部模块，如相对路径的模块、包名前缀为 @ 的模块。
              unknown ：未知模块，如模块名没有指定扩展名或模块路径缺失扩展名。
              parent ：父级目录的模块。
              sibling ：同级目录的模块。
              index ：当前目录的 index 文件。
              object ：使用ES6 导入的模块。
              type ：使用 import type 导入的模块。
            */
            ["builtin", "external"],
            "internal",
            ["parent", "sibling"],
            "index",
            "type",
            "object",
            "unknown",
          ],
          pathGroups: [
            {
              pattern: "../**",
              group: "parent",
              position: "after",
            },
            {
              pattern: "./*.scss",
              group: "sibling",
              position: "after",
            },
          ],
          // 不同组之间是否换行。
          "newlines-between": "always",
          // 根据字母顺序对每组内的引用进行排序。
          alphabetize: {
            order: "asc",
            caseInsensitive: true,
          },
        },
      ],
    },
  };
  ```

- 根目录新建忽略文件 `.eslintignore`

```js
dist
node_modules
public
.husky
.vscode
.idea
*.sh
*.md

index.html

.eslintrc.cjs
.prettierrc.json
.stylelintrc.cjs
```
### 4. 保存自动格式化当前文件

![image-20240207100527778](imgs/image-20240207100527778.png)

- 根目录下新建 .vscode/settings.json

```js
// settings.json

{
  "typescript.tsdk": "node_modules/typescript/lib",
  "editor.defaultFormatter": "esbenp.prettier-vscode",
  "editor.formatOnSave": true,
  "editor.codeActionsOnSave": {
    "source.fixAll.eslint": "explicit",
    "source.fixAll.stylelint": "explicit"
  },
  "stylelint.validate": ["css", "scss", "less", "vue"],
  "[vue]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  },
  "[ts]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  },
  "[javascript]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  },
  "[scss]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  }
}
```
## 三、 stylelint 配置 
>[Airbnb CSS/Sass 代码规范](https://github.com/Zhangjd/css-style-guide?tab=readme-ov-file#oocss-and-bem)

### 1. vscode 安装插件Stylelint

![image-20240207101521445](imgs/image-20240207101521445.png)

### 项目安装插件

```js
pnpm i stylelint stylelint-config-standard stylelint-config-recommended-scss stylelint-config-recommended-vue postcss postcss-html postcss-scss stylelint-config-recess-order stylelint-config-html -D
```



| 依赖 | 说明 | 备注 |
| :-- | :-- | :-- |
| stylelint | stylelint 核心库 | stylelint |
| stylelint-config-standard | Stylelint 标准共享配置 | stylelint-config-standard 文档 |
| stylelint-config-recommended-scss | 扩展 stylelint-config-recommended 共享配置并为 SCSS 配置其规则 | stylelint-config-recommended-scss 文档 |
| stylelint-config-recommended-vue  | 扩展 stylelint-config-recommended 共享配置并为 Vue 配置其规则 | stylelint-config-recommended-vue 文档 |
| stylelint-config-recess-order | 提供优化样式顺序的配置 | CSS 书写顺序规范 |
| stylelint-config-html | 共享 HTML (类似 HTML) 配置，捆绑 postcss-html 并对其进行配置 | stylelint-config-html 文档 |
| postcss-html | 解析 HTML (类似 HTML) 的 PostCSS 语法 | postcss-html 文档 |
| postcss-scss | PostCSS 的 SCSS 解析器 | postcss-scss 文档，支持 CSS 行类注释 |

### 2. 根目录下新建规则配置文件

-  .stylelintrc.cjs

  ```js
  module.exports = {
    // 继承推荐规范配置
    extends: [
      'stylelint-config-standard',
      'stylelint-config-recommended-scss',
      'stylelint-config-recommended-vue/scss',
      'stylelint-config-html/vue',
      'stylelint-config-recess-order'
    ],
    // 指定不同文件对应的解析器
    overrides: [
      {
        files: ['**/*.{vue,html}'],
        customSyntax: 'postcss-html'
      },
      {
        files: ['**/*.{css,scss}'],
        customSyntax: 'postcss-scss'
      }
    ],
    // 自定义规则
    rules: {
      // 允许 global 、export 、v-deep等伪类
      'selector-pseudo-class-no-unknown': [
        true,
        {
          ignorePseudoClasses: ['global', 'export', 'v-deep', 'deep']
        }
      ],
      'selector-class-pattern': null,
      // 'selector-no-vendor-prefix': null,
      // 'value-no-vendor-prefix': null,
      // 'alpha-value-notation': null,
      'color-function-notation': null,
      // 'rule-empty-line-before': null,
      'no-descending-specificity': null,
      // 'number-leading-zero': null,
      // 'declaration-block-no-redundant-longhand-properties': null,
      'font-family-no-missing-generic-family-keyword': null
    }
  }
  ```

- .stylelintignore

  ```js
  dist
  node_modules
  public
  .husky
  .vscode
  .idea
  *.sh
  *.md
  
  stats.html
  ```

### 3. 新增命令

- package.json

  ```js
   "scripts": {
      // ...
      "lint:stylelint": "stylelint  \"**/*.{css,scss,vue,html}\" --fix",
    },
  ```
## 四、 代码提交检查

Husky + Lint-staged + Commitlint + Commitizen + cz-git  来配置 Git 提交代码规范。

> 核心内容是配置 Husky 的 pre-commit 和 commit-msg 两个钩子:
>
> pre-commit：Husky + Lint-staged 整合实现 Git 提交前代码规范检测/格式化 (前提：ESlint + Prettier + Stylelint 代码统一规范)；
>
> commit-msg: Husky + Commitlint + Commitizen + cz-git 整合实现生成规范化且高度自定义的 Git commit message。
>

### 1. husky  

Husky 是 Git 钩子工具，可以设置在 git 各个阶段（`pre-commit`、`commit-msg` 等）触发。

官网https://typicode.github.io/husky 推荐安装指令

- 1 前提条件 项目有.git 如果没有需要生成

  ```js
  git init
  ```

- 2 自动配置husky

  ```js
  npx husky-init
  ```

  ![image-20240207141214268](imgs/image-20240207141214268.png)

- 3 安装husky 执行pnpm i

  ```js
  pnpm i
  ```

  ![image-20240207141258999](imgs/image-20240207141258999.png)

### 2. Lint-staged 增量检测提交代码

lint-staged 是一个在 git add 到暂存区的文件运行 linters (ESLint/Prettier/StyleLint) 的工具，避免在 git commit 提交时在整个项目执行。

- 1 安装

  ```js
  pnpm i lint-staged -D
  ```

- 2 package.json 中添加不同文件在 git 提交执行的 lint 检测配置

  ```js
  // package.json
  {
      //...
       "lint-staged": {
          "*.{js,ts}": [
            "eslint --fix",
            "prettier --write"
          ],
          "*.{cjs,json}": [
            "prettier --write"
          ],
          "*.{vue,html}": [
            "eslint --fix",
            "prettier --write",
            "stylelint --fix --allow-empty-input"
          ],
          "*.{scss,css}": [
            "stylelint --fix --allow-empty-input",
            "prettier --write"
          ]
        }
  }
  ```

- 3 `package.json`添加指令

  ```js
   "scripts": {
     	// ...
      "lint:lint-staged": "lint-staged",
    },
  ```

- 4   `.husky/pre-commit`**修改提交前钩子命令**

  ```js
  #npm test
  npm run lint:lint-staged --allow-empty
  ```

  ![image-20240207143016869](imgs/image-20240207143016869.png)

### 3. Commitlint 

Commitlint 检查您的提交消息是否符合 Conventional commit format。-- [Commitlint 官网](https://commitlint.js.org/)

- 1 安装

  ```js
  pnpm i @commitlint/cli @commitlint/config-conventional -D
  ```

- 2 根目录创建 `commitlint.config.cjs` 配置文件

  ```js
  module.exports = {
    // 继承的规则
    extends: ['@commitlint/config-conventional'],
    // @see: https://commitlint.js.org/#/reference-rules
    rules: {
      'subject-case': [0], // subject大小写不做校验
  
      // 类型枚举，git提交type必须是以下类型
      'type-enum': [
        2,
        'always',
        [
          'feat', // 新增功能
          'fix', // 修复缺陷
          'docs', // 文档变更
          'style', // 代码格式（不影响功能，例如空格、分号等格式修正）
          'refactor', // 代码重构（不包括 bug 修复、功能新增）
          'perf', // 性能优化
          'test', // 添加疏漏测试或已有测试改动
          'build', // 构建流程、外部依赖变更（如升级 npm 包、修改 webpack 配置等）
          'ci', // 修改 CI 配置、脚本
          'revert', // 回滚 commit
          'chore' // 对构建过程或辅助工具和库的更改（不影响源文件、测试用例）
        ]
      ]
    }
  }
  ```

- 3  执行下面命令生成 `commint-msg` 钩子用于 git 提交信息校验

  ```js
  npx husky add .husky/commit-msg "npx --no -- commitlint --edit $1"
  ```

  ![image-20240207142542813](imgs/image-20240207142542813.png)

## 五、 Commitizen & cz-git 【额外提交规范 非必要】

**commitizen**: 基于Node.js的 `git commit` 命令行工具，辅助生成标准化规范化的 commit message。–[官方文档](https://github.com/commitizen/cz-cli)

**cz-git**: 一款工程性更强，轻量级，高度自定义，标准输出格式的 commitizen 适配器。–[官方文档](https://cz-git.qbb.sh/zh/)

- 安装

  ```js
  pnpm i commitizen cz-git -D
  ```

- **修改 `package.json` 指定使用的适配器**

  ```js
  {
      // ...
       "config": {
          "commitizen": {
            "path": "node_modules/cz-git"
          }
        }
  }
  ```

- 修改 `commitlint.config.cjs`

  ```js
  /* eslint-env node */
  module.exports = {
    // 继承的规则
    extends: ['@commitlint/config-conventional'],
    // 自定义规则
    rules: {
      // @see https://commitlint.js.org/#/reference-rules
  
      // 提交类型枚举，git提交type必须是以下类型
      'type-enum': [
        2,
        'always',
        [
          'feat', // 新增功能
          'fix', // 修复缺陷
          'docs', // 文档变更
          'style', // 代码格式（不影响功能，例如空格、分号等格式修正）
          'refactor', // 代码重构（不包括 bug 修复、功能新增）
          'perf', // 性能优化
          'test', // 添加疏漏测试或已有测试改动
          'build', // 构建流程、外部依赖变更（如升级 npm 包、修改 webpack 配置等）
          'ci', // 修改 CI 配置、脚本
          'revert', // 回滚 commit
          'chore' // 对构建过程或辅助工具和库的更改（不影响源文件、测试用例）
        ]
      ],
      'subject-case': [0] // subject大小写不做校验
    },
  
    prompt: {
      messages: {
        type: '选择你要提交的类型 :',
        scope: '选择一个提交范围（可选）:',
        customScope: '请输入自定义的提交范围 :',
        subject: '填写简短精炼的变更描述 :\n',
        body: '填写更加详细的变更描述（可选）。使用 "|" 换行 :\n',
        breaking: '列举非兼容性重大的变更（可选）。使用 "|" 换行 :\n',
        footerPrefixesSelect: '选择关联issue前缀（可选）:',
        customFooterPrefix: '输入自定义issue前缀 :',
        footer: '列举关联issue (可选) 例如: #31, #I3244 :\n',
        generatingByAI: '正在通过 AI 生成你的提交简短描述...',
        generatedSelectByAI: '选择一个 AI 生成的简短描述:',
        confirmCommit: '是否提交或修改commit ?'
      },
      // prettier-ignore
      types: [
        { value: "feat",     name: "特性:     ✨  新增功能", emoji: ":sparkles:" },
        { value: "fix",      name: "修复:     🐛  修复缺陷", emoji: ":bug:" },
        { value: "docs",     name: "文档:     📝  文档变更", emoji: ":memo:" },
        { value: "style",    name: "格式:     🌈  代码格式（不影响功能，例如空格、分号等格式修正）", emoji: ":lipstick:" },
        { value: "refactor", name: "重构:     🔄  代码重构（不包括 bug 修复、功能新增）", emoji: ":recycle:" },
        { value: "perf",     name: "性能:     🚀  性能优化", emoji: ":zap:" },
        { value: "test",     name: "测试:     🧪  添加疏漏测试或已有测试改动", emoji: ":white_check_mark:"},
        { value: "build",    name: "构建:     📦️  构建流程、外部依赖变更（如升级 npm 包、修改 vite 配置等）", emoji: ":package:"},
        { value: "ci",       name: "集成:     ⚙️  修改 CI 配置、脚本",  emoji: ":ferris_wheel:"},
        { value: "revert",   name: "回退:     ↩️  回滚 commit",emoji: ":rewind:"},
        { value: "chore",    name: "其他:     🛠️  对构建过程或辅助工具和库的更改（不影响源文件、测试用例）", emoji: ":hammer:"},
      ],
      useEmoji: true,
      emojiAlign: 'center',
      useAI: false,
      aiNumber: 1,
      themeColorCode: '',
      scopes: [],
      allowCustomScopes: true,
      allowEmptyScopes: true,
      customScopesAlign: 'bottom',
      customScopesAlias: 'custom',
      emptyScopesAlias: 'empty',
      upperCaseSubject: false,
      markBreakingChangeMode: false,
      allowBreakingChanges: ['feat', 'fix'],
      breaklineNumber: 100,
      breaklineChar: '|',
      skipQuestions: [],
      issuePrefixes: [{ value: 'closed', name: 'closed:   ISSUES has been processed' }],
      customIssuePrefixAlign: 'top',
      emptyIssuePrefixAlias: 'skip',
      customIssuePrefixAlias: 'custom',
      allowCustomIssuePrefix: true,
      allowEmptyIssuePrefix: true,
      confirmColorize: true,
      maxHeaderLength: Infinity,
      maxSubjectLength: Infinity,
      minSubjectLength: 0,
      scopeOverrides: undefined,
      defaultBody: '',
      defaultIssues: '',
      defaultScope: '',
      defaultSubject: ''
    }
  }
  ```

- `package.json` 添加 `commit` 指令

  ```js
    "scripts": {
    	// ...
      "commit": "git-cz"
    },
  ```

- 执行验证

  ```js
  // 代码添加暂存区
  git add . 
  ```

  ```js
  // 验证
  pnpm commit
  ```

  ![image-20240207154927877](imgs/image-20240207154927877.png)