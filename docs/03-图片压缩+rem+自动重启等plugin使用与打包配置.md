## 一、`Svg`配置 
> 每次引入一张 `SVG` 图片都需要写一次相对路径，并且对 `SVG` 图片进行压缩优化也不够方便。
> `vite-svg-loader`插件加载`SVG`文件作为`Vue`组件，使用`SVGO`进行优化。

- 插件网站[https://www.npmjs.com/package/vite-svg-loader](https://www.npmjs.com/package/vite-svg-loader)

### 1. 安装
```js
pnpm i vite-svg-loader -D
```

### 2. `vite.config.ts`配置
```js
// svg加载
import svgLoader from 'vite-svg-loader'

// https://vitejs.dev/config/
export default defineConfig({
  // ...
  plugins: [
    // ...
    svgLoader({
      defaultImport: 'url', // or 'raw'
      svgo: true
    })
  ],
})
```

### 3. 使用
```js
<template>
  <img w20 h20 :src="sunUrl" />
</template>

<script setup lang="ts">
import sunUrl from '@/assets/svg/sun.svg'
</script>
```
### 4. 注意
如果使用默认`url`使用`svg`图片的话，和图片压缩`vite-plugin-image-optimizer`中的`svgo`其实还是有重复的


## 二、`gzip`压缩
当前端资源过大时，服务器请求资源会比较慢。前端可以将资源通过Gzip压缩使文件体积减少大概60%左右，压缩后的文件，通过后端简单处理，浏览器可以将其正常解析出来。
- 安装
```js
pnpm i vite-plugin-compression -D
```
- `vite.config.ts`配置
```js
// 压缩gzip
import viteCompression from 'vite-plugin-compression'

// https://vitejs.dev/config/
export default defineConfig({
  // ...
  plugins: [
    // ...
    viteCompression({
        verbose: true, // 默认即可
        disable: false, // 开启压缩(不禁用)，默认即可
        deleteOriginFile: false, // 删除源文件
        threshold: 10240, // 压缩前最小文件大小
        algorithm: 'gzip', // 压缩算法
        ext: '.gz' // 文件类型
    })
  ],
})
```
- 效果图

![alt text](./imgs/viteCompression.png)


## 三、 自动重启
> 对`config.js/config.ts`等配置文件修改后重启

插件网站[https://www.npmjs.com/package/vite-plugin-restart](https://www.npmjs.com/package/vite-plugin-restart)

- 安装
```js
pnpm i vite-plugin-restart -D
```
- `vite.config.ts`配置
```js
// 自动重启
import ViteRestart from 'vite-plugin-restart'

// https://vitejs.dev/config/
export default defineConfig({
  // ...
  plugins: [
    // ...
    ViteRestart({
       restart: ['*.config.[jt]s', '**/config/*.[jt]s', '*.config.cjs']
    })
  ],
})
```

## 四、`rem`转换(`vite-plugin-px2rem`)

官网[https://github.com/ch1ny/vite-plugin-px2rem/blob/HEAD/README-zh_CN.md](https://github.com/ch1ny/vite-plugin-px2rem/blob/HEAD/README-zh_CN.md)

### 1. 安装
```js
# npm
npm install vite-plugin-px2rem --save-dev
# 或 yarn
yarn add vite-plugin-px2rem -D
# 或 pnpm
pnpm add vite-plugin-px2rem -D
```
一个支持将你的样式表中的 `px` 转换成 `rem` 的 `vite` 插件。
支持 `css`、`less` 以及 `sass/scss`。
> 不支持对 `js/ts` 文件内部的代码进行转换。

### 2. `vite.config.ts`配置
```js
// vite.config.ts
import { defineConfig } from 'vite';
import { px2rem } from 'vite-plugin-px2rem';

// https://vitejs.dev/config/
export default defineConfig({
	plugins: [
		px2rem({
			width: 750,
			rootFontSize: 16,
		}),
	],
});
```

## 五、图片压缩(`vite-plugin-image-optimizer`)
> - 使用`SVGO`优化`SVG`资产并传递自定义配置。
> - 使用`Sharp.js`优化标量资产(`png, jpeg, gif, tiff, webp, avif`)，并可以为每个扩展类型传递自定义配置。
> - 选项来处理在打包器中定义的公共目录中的所有资产。
> - 配置`test`、`include`和`exclude`来过滤资产。
> - **如果资产的优化大小大于原始大小，则跳过处理资产**。
> - 记录优化统计信息，显示前后的大小差异、比例和总节省(可选)

### 1. 安装

官网[https://www.npmjs.com/package/vite-plugin-image-optimizer](https://www.npmjs.com/package/vite-plugin-image-optimizer)

```
pnpm i vite-plugin-image-optimizer --save-dev
pnpm i sharp --save-dev
pnpm i svgo --save-dev
```
> `Sharp`和`svgo`不作为软件包的一部分来安装。您必须手动安装它们并将其添加为开发依赖项。这是一个设计决策，所以如果您只想使用`svgo`优化`svg`资产，则可以选择跳过安装`sharp`，反之亦然。

### 2. `vite.config.ts`配置
```js
// 图片压缩
import { ViteImageOptimizer } from 'vite-plugin-image-optimizer'

export default defineConfig({
  // ...
  plugins: [
    // ...
    // 图片压缩
    ViteImageOptimizer()
  ],
})
```

## 六、打包分析

官网[https://www.npmjs.com/package/rollup-plugin-visualizer](https://www.npmjs.com/package/rollup-plugin-visualizer)

### 1. 安装
```js
pnpm i rollup-plugin-visualizer -D
```

### 2. `vite.config.ts`配置
```js
import { visualizer } from 'rollup-plugin-visualizer'

// https://vitejs.dev/config/
export default defineConfig({
  // ...
  plugins: [
    // ...
   visualizer({
        open: true, // 注意这里要设置为true，否则无效
        gzipSize: true,
        brotliSize: true
  	})
  ],
})
```

## 七、打包拆分 小图片转`base64`
- `vite.config.ts`配置
```js
// https://vitejs.dev/config/
export default defineConfig({
  // ...
  build:{
    // 10kb以下，转Base64
    assetsInlineLimit: 1024 * 10,
    // chunkSizeWarningLimit: 1500,//配置文件大小提醒限制，默认500
    rollupOptions: {
      output: {
        // 每个node_modules模块分成一个js文件
        manualChunks(id: string) {
          if (id.includes('node_modules')) {
            // return 'vendor'
            return  id.toString().split('node_modules/.pnpm/')[1].split('/')[0].toString()
          }
          return undefined
        },
        // 用于从入口点创建的块的打包输出格式[name]表示文件名,[hash]表示该文件内容hash值
        entryFileNames: 'assets/js/[name].[hash].js', // 用于命名代码拆分时创建的共享块的输出命名
        chunkFileNames: 'assets/js/[name].[hash].js', // 用于输出静态资源的命名，[ext]表示文件扩展名
        assetFileNames: 'assets/[ext]/[name].[hash].[ext]'
      }
    }
  }
})
```

## 八、打包进度
> `vite-plugin-progress` 插件是一个在打包时展示进度条的插件，如果您觉得该插件对您的项目有帮助

插件网站[https://www.npmjs.com/package/vite-plugin-progress](https://www.npmjs.com/package/vite-plugin-progress)

- 安装
```js
pnpm i vite-plugin-progress -D
```
- `vite.config.ts`配置
```js
// 打包进度
import progress from 'vite-plugin-progress'

// https://vitejs.dev/config/
export default defineConfig({
  // ...
  plugins: [
    // ...
    progress()
  ],
})
```

## 九、完整代码 `vite.config.ts`
```js
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import { fileURLToPath, URL } from 'node:url'
// import AutoImport from 'unplugin-auto-import/vite'
// 打包分析
import { visualizer } from 'rollup-plugin-visualizer'
import UnoCSS from 'unocss/vite'
import { defineConfig } from 'vite'
// 压缩gzip
import viteCompression from 'vite-plugin-compression'
// 打包进度
// import progress from 'vite-plugin-progress'
// 压缩图片
import { ViteImageOptimizer } from 'vite-plugin-image-optimizer'
// rem转换
import { px2rem } from 'vite-plugin-px2rem'
// 自动重启
import ViteRestart from 'vite-plugin-restart'
import VueDevTools from 'vite-plugin-vue-devtools'
// svg配置
import svgLoader from 'vite-svg-loader'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    vueJsx(),
    VueDevTools(),
    // AutoImport({
    //   include: [
    //     /\.[tj]sx?$/, // .ts, .tsx, .js, .jsx
    //     /\.vue$/,
    //     /\.vue\?vue/, // .vue
    //     /\.md$/ // .md
    //   ],
    //   imports: ['vue', 'vue-router', 'pinia'],
    //   defaultExportByFilename: true,
    //   vueTemplate: true,
    //   dts: './types/auto-imports.d.ts',
    //   // eslint 报错解决：'ref' is not defined
    //   eslintrc: {
    //     enabled: true, // Default `false`
    //     filepath: './.eslintrc-auto-import.json', // Default `./.eslintrc-auto-import.json`
    //     globalsPropValue: true
    //   }
    // })
    UnoCSS(),
    viteCompression({
      verbose: true, // 默认即可
      disable: false, // 开启压缩(不禁用)，默认即可
      deleteOriginFile: false, // 删除源文件
      threshold: 1024, // 压缩前最小文件大小
      algorithm: 'gzip', // 压缩算法
      ext: '.gz' // 文件类型
    }),
    // progress(),
    ViteRestart({
      restart: ['*.config.[jt]s', '**/config/*.[jt]s', '*.config.cjs']
    }),
    svgLoader({
      defaultImport: 'url' // or 'raw'
    }),
    visualizer({
      open: true, // 注意这里要设置为true，否则无效
      gzipSize: true,
      brotliSize: true
    }),
    px2rem({
      width: 750,
      rootFontSize: 16
    }),
    ViteImageOptimizer({
      /* pass your config */
    })
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  css: {
    preprocessorOptions: {
      scss: {
        additionalData: `
          @use "./src/styles/variables.scss" as *;
          @use "./src/styles/mixin.scss" as *;`,
        javascriptEnabled: true
      }
    }
  },
  server: {
    // 监听所有公共ip
    // host: '0.0.0.0',
    cors: true,
    port: 3300,
    proxy: {
      // 前缀
      '/api': {
        target: 'http://www.example.com',
        changeOrigin: true,
        // 前缀重写
        rewrite: (path: string) => path.replace(/^\/api/, '/api')
      }
    }
  },
  // ...
  build: {
    // 10kb以下，转Base64
    assetsInlineLimit: 1024 * 10,
    // chunkSizeWarningLimit: 1500,//配置文件大小提醒限制，默认500
    rollupOptions: {
      output: {
        // 每个node_modules模块分成一个js文件
        manualChunks(id: string) {
          if (id.includes('node_modules')) {
            return 'vendor'
            // return id.toString().split('node_modules/.pnpm/')[1].split('/')[0].toString()
          }
          return undefined
        },
        // 用于从入口点创建的块的打包输出格式[name]表示文件名,[hash]表示该文件内容hash值
        entryFileNames: 'assets/js/[name].[hash].js', // 用于命名代码拆分时创建的共享块的输出命名
        chunkFileNames: 'assets/js/[name].[hash].js', // 用于输出静态资源的命名，[ext]表示文件扩展名
        assetFileNames: 'assets/[ext]/[name].[hash].[ext]'
      }
    }
  }
})
```



