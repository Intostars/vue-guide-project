
## 一、环境变量配置
官网[https://cn.vitejs.dev/guide/env-and-mode.html#intellisense](https://cn.vitejs.dev/guide/env-and-mode.html#intellisense)

### 1. 新建`.env`开头的文件在根目录
  为了防止意外地将一些环境变量泄漏到客户端，只有以 `VITE_` 为前缀的变量才会暴露给经过 `vite` 处理的代码

  > `.env` 所有环境默认加载
  > `.env.development` 开发模式默认加载
  > `.env.production` 生产模式默认加载
  > `.env.check` 自定义环境文件

示例：如`.env`文件
```bash
# title
VITE_APP_TITLE = vue-guide-project
```
#### a. 模式
默认情况下，开发服务器 (`dev` 命令) 运行在 `development` (开发) 模式，而 `build` 命令则运行在 `production` (生产) 模式。
这意味着当执行 `vite build` 时，它会自动加载 `.env.production` 中可能存在的环境变量。
在某些情况下，若想在 `vite build` 时运行不同的模式来渲染不同的标题，可以通过传递 `--mode` 选项标志来覆盖命令使用的默认模式。例如，如果你想在 `staging` （预发布）模式下构建应用：

```bash
vite build --mode staging

# package.json
{
  # ...
  "type": "module",
  "scripts": {
    "dev": "vite",
    "build": "vite build --mode staging",
  },
}
```
还需要新建一个 `.env.staging` 文件：

```js
# .env.staging
VITE_APP_TITLE = My App (staging)
```
#### b. 简单了解下`NODE_ENV`和模式`Mode`
- `NODE_ENV`：
定义：技术上讲，`NODE_ENV`是一个由`Node.js`暴露给执行脚本的系统环境变量。
作用：通常用于确定服务器工具、构建脚本和客户端`library`在开发环境（`development`）还是生产环境（`production`）下的行为。它的值通常为"`production`"或"`development`"，用于区分不同环境下的逻辑行为。
- 模式（`Mode`）：
定义：模式指项目运行或构建时的一种特定状态或配置。
作用：在前端框架中，模式用于定义项目在不同阶段（如开发、生产、测试）下的行为和配置。它可以包含多个环境变量，并通过特定的文件（如`.env.production`、`.env.development`等）来管理这些变量。

所以，我们所写的`.env`可以理解为创建的不同的模式变量

### 2. `TypeScript` 智能提示，设置全局类型定义
> 就是在编码过程中应用这些自定义环境变量的时候，给出的智能提示。

1. `src` 目录下创建一个 `vite-env.d.ts` 或者 `env.d.ts` 文件
2. `src`同级别`types`目录下创建文件`env.d.ts`
```ts
/// <reference types="vite/client" />

interface ImportMetaEnv {
  readonly VITE_APP_TITLE: string
  // 更多环境变量...
}

interface ImportMeta {
  readonly env: ImportMetaEnv
}

interface ViteEnv  extends ImportMetaEnv {}
```
3. 在`tsconfig.app.json`文件中
> 专门用于处理项目`src`文件中的`TypeScript`配置文件，
- `include`配置项加入文件：(会提示自定义设置的环境变量)
```json
"include": [
  // ...
  // 第一种方式对应配置
  "vite-env.d.ts", // 或者 "env.d.ts" 
  // 第二种方式对应配置
  "types/**.d.ts" // 或者直接 "types" 
],
```
> 效果图：![alt text](./imgs/env/tsconfig_include.png)
- 或者`compilerOptions`中加入`types`:(只会提示默认环境变量)
```json
{
  "compilerOptions": {
    // ...
    "types": ["vite/client"]
  }
}
```
> 效果图：![alt text](./imgs/env/tsconfig_types.png)

### 3. 访问环境变量
1. 客户端源码(就是`src`文件)中访问：`import.meta.env.VITE_APP_TITLE`
2. `html`中访问环境变量：`%VITE_APP_TITLE%`,如果环境变量不存在，则会将被忽略而不被替换，
3. 在配置(`src`文件外)文件中访问环境变量
> `Vite` 默认是不加载 `.env` 文件的，因为这些文件需要在执行完 `Vite` 配置后才能确定加载哪一个，举个例子，`root` 和 `envDir` 选项会影响加载行为。不过当你的确需要时，你可以使用 `Vite` 导出的 `loadEnv` 函数来加载指定的 `.env` 文件。
```js
import { defineConfig, loadEnv } from 'vite'
export default defineConfig(({ command, mode }) => {
  // command: "build" | "serve" 
  // mode 当前模式
  // 根据当前工作目录中的 `mode` 加载 .env 文件
  // 设置第三个参数为 '' 来加载所有环境变量，而不管是否有 `VITE_` 前缀。
  const env = loadEnv(mode, process.cwd(), '')
  return {
    // vite 配置
    define: {
      __APP_ENV__: JSON.stringify(env.APP_ENV),
    },
  }
})
```
输出：`console.log(env)`
![alt text](./imgs/env/loadEnv.png)


## 二、情景配置-根据不同模式使用不同插件

### 1. 新建`build/getEnv.ts` 文件处理环境文件变量
> `loadEnv()`获取的环境变量`env`从输出的值可以看出，全是字符串，所以我们可以自定义方法去转换变量类型
```js
/* eslint-disable */
// Read all environment variable configuration files to process.env
export function wrapperEnv(envConf: any): ViteEnv {
  const ret: any = {}

  for (const envName of Object.keys(envConf)) {
    let realName = envConf[envName].replace(/\\n/g, '\n')
    realName = realName === 'true' ? true : realName === 'false' ? false : realName

    if (envName === 'VITE_PORT') {
      realName = Number(realName)
    }
    if (envName === 'VITE_PROXY' && realName) {
      try {
        realName = JSON.parse(realName.replace(/'/g, '"'))
      } catch (error) {
        realName = ''
      }
    }
    ret[envName] = realName
  }
  return ret
}
```

### 2. `build/plugins/index.ts` 
新建`build/plugins` 文件夹处理各种`plugins`，将每一个`plugin`配置单独抽离
- `plugins`文件目录
![alt text](./imgs/env/plugin.png)
- 入口文件`build/plugins/index.ts`
```js
// 插件配置 入口文件index.ts
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import { PluginOption } from 'vite'
import VueDevTools from 'vite-plugin-vue-devtools'

import { Px2remPlugin } from './Px2rem' // rem
import { svgLoaderPlugin } from './SvgLoader' // SVG
import { UnocssPlugin } from './Unocss' // unocss
import { VisualizerPlugin } from './Visualizer' // 打包分析
import { ViteCompressionPlugin } from './ViteCompression' // 压缩gzip
import { ViteImageOptimizerPlugin } from './ViteImageOptimizer' // 图片压缩
import { ViteRestartPlugin } from './ViteRestartPlugin' // 修改配置文件自动重启

export const usePlugins = (isBuild: boolean, viteEnv: ViteEnv) => {
  const { VITE_OPEN_VISUALIZER, VITE_OPEN_SVG_LOADER, VITE_OPEN_PX2REM, VITE_OPEN_COMPRESSION } =
    viteEnv
  const plugins: PluginOption[] = [vue(), vueJsx()]

  plugins.push(UnocssPlugin())
  if (VITE_OPEN_PX2REM) plugins.push(Px2remPlugin())
  if (VITE_OPEN_SVG_LOADER) plugins.push(svgLoaderPlugin())

  // 开发模式下
  if (!isBuild) {
    plugins.push(VueDevTools())
    plugins.push(ViteRestartPlugin())
  }

  if (isBuild) {
    plugins.push(ViteImageOptimizerPlugin())
    // 压缩gzip
    VITE_OPEN_COMPRESSION && plugins.push(ViteCompressionPlugin())
    // 打包分析
    VITE_OPEN_VISUALIZER && plugins.push(VisualizerPlugin())
  }
  return plugins
}

```

- 单个`plugin`示例：
![alt text](./imgs/env/visualizer.png)

### 4. `tsconfig.node.json`
>专门用于`Node.js`环境中的`TypeScript`配置文件，它定义了用于`Node.js`应用程序的`TypeScript`编译器选项

`tsconfig.node.json`文件中：
```js
"include": [
  // ...
  "build/**/*.ts",
  "types" // 全局类型
],
```
### 5. `.eslintrc.cjs`中添加：
```ts
overrides: [
  {
    files: ['*.ts', '*.tsx', '*.vue'],
    rules: {
      // 解决 ts 全局类型定义后，eslint报错的问题
      'no-undef': 'off'
    }
  }
],
// ...
'no-unused-expressions': 'off' // 关闭禁止使用表达式
```

### 3. `vite.config.ts`中
```js
import { usePlugins } from './build/plugins'
// ...
export default defineConfig(({ command, mode }) => {
  const isBuild = command === 'build'
  const root = process.cwd()
  const env = loadEnv(mode, root)
  const viteEnv = wrapperEnv(env)
  return {
    plugins: usePlugins(isBuild, viteEnv),
    // ...
  }
})
```

## 三、情景配置-`server`和`build`配置
`build`文件夹中创建`server.ts`和`build.ts`文件
### 1. 将`build`配置写入`build.ts`文件中
```js
export const useBuild = () => {
  return {
    // 10kb以下，转Base64
    assetsInlineLimit: 1024 * 10,
    // chunkSizeWarningLimit: 1500,//配置文件大小提醒限制，默认500
    rollupOptions: {
      output: {
        // 每个node_modules模块分成一个js文件
        manualChunks(id: string) {
          if (id.includes('node_modules')) {
            return 'vendor'
            // return id.toString().split('node_modules/.pnpm/')[1].split('/')[0].toString()
          }
          return undefined
        },
        // 用于从入口点创建的块的打包输出格式[name]表示文件名,[hash]表示该文件内容hash值
        entryFileNames: 'assets/js/[name].[hash].js', // 用于命名代码拆分时创建的共享块的输出命名
        chunkFileNames: 'assets/js/[name].[hash].js', // 用于输出静态资源的命名，[ext]表示文件扩展名
        assetFileNames: 'assets/[ext]/[name].[hash].[ext]'
      }
    }
  }
}
```

### 2. 将`server`配置写入`server.ts`文件中
```js
import type { ProxyOptions } from 'vite'

type ProxyItem = [string, string]
type ProxyList = ProxyItem[]
type ProxyTargetList = Record<string, ProxyOptions>

/**
 * 创建代理，用于解析 .env.development 代理配置
 */
const userProxy = (proxyList: ProxyList = []) => {
  const ret: ProxyTargetList = {}

  proxyList.forEach((item) => {
    const [prefix, target] = item
    const httpsRE = /^https:\/\//
    const isHttps = httpsRE.test(target)
    ret[prefix] = {
      target,
      changeOrigin: true,
      ws: true,
      rewrite: (path: string) => path.replace(new RegExp(`^${prefix}`), ''),
      // https is require secure=false
      // Verify SSL certificate
      ...(isHttps ? { secure: false } : {})
    }
  })

  return ret
}

/**
 * server 配置
 * @returns
 */
export const useServer = (viteEnv: ViteEnv) => {
  const { VITE_PORT, VITE_PROXY } = viteEnv
  return {
    // 监听所有公共ip
    // host: '0.0.0.0',
    cors: true,
    port: VITE_PORT,
    proxy: userProxy(VITE_PROXY)
  }
}

```

### 3. 完整`vite.config.ts`文件
```js
import { fileURLToPath, URL } from 'node:url'
import { defineConfig, loadEnv } from 'vite'

import { useBuild } from './build/build'
import { wrapperEnv } from './build/getEnv'
import { usePlugins } from './build/plugins'
import { useServer } from './build/server'

export default defineConfig(({ command, mode }) => {
  const isBuild = command === 'build'
  const root = process.cwd()
  const env = loadEnv(mode, root)
  const viteEnv = wrapperEnv(env)
  return {
    plugins: usePlugins(isBuild, viteEnv),
    server: useServer(viteEnv),
    build: useBuild(),
    resolve: {
      alias: {
        '@': fileURLToPath(new URL('./src', import.meta.url))
      }
    },
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: `
          @use "./src/styles/variables.scss" as *;
          @use "./src/styles/mixin.scss" as *;`,
          javascriptEnabled: true
        }
      }
    }
  }
})

```