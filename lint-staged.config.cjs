module.exports = {
  '*.{js,ts}': ['eslint --fix', 'prettier --write'],
  '*.{cjs,json}': ['prettier --write'],
  '*.{vue,html}': ['eslint --fix', 'prettier --write', 'stylelint --fix --allow-empty-input'],
  '*.{scss,css}': ['stylelint --fix --allow-empty-input', 'prettier --write']
}
