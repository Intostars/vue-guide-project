// 创建App实例工厂函数
import { createApp } from 'vue'

// 引入根组件
import useRouter from '@/router'
import usePinia from '@/store'

// 引入全局样式
import '@/styles/index.scss'
// eslint-disable-next-line import/no-unresolved
import 'virtual:uno.css'

import App from './App.vue' // uno.css

console.log(import.meta.env)
// 初始化app实例
const app = createApp(App)
// 使用router
useRouter(app)
// 使用pinia
usePinia(app)
// app实例挂载到id为app的元素上
app.mount('#app')
