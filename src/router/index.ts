import { createRouter, createWebHistory } from 'vue-router'

import type { App } from 'vue'
import type { RouteRecordRaw } from 'vue-router'

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    redirect: '/home'
  },
  {
    path: '/home',
    component: () => import('@/views/home/index.vue')
  },
  {
    path: '/about',
    component: () => import('@/views/about/index.vue')
  },
  {
    path: '/unocss',
    component: () => import('@/views/unocss/index.vue')
  },
  {
    path: '/svg',
    component: () => import('@/views/svg/index.vue')
  },
  {
    path: '/rem',
    component: () => import('@/views/rem/index.vue')
  },
  {
    path: '/images',
    component: () => import('@/views/images/index.vue')
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

const useRouter = (app: App) => {
  app.use(router)
}
export default useRouter
