import { createPinia } from 'pinia'
// 持久化pinia插件
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'

import type { App } from 'vue'

// 创建pinia实例
const pinia = createPinia()

// pinia持久化插件
pinia.use(piniaPluginPersistedstate)

// 函数式注入pinia
const usePinia = (app: App) => {
  app.use(pinia)
}

export default usePinia
