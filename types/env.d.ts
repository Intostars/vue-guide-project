/// <reference types="vite/client" />

interface ImportMetaEnv {
  readonly VITE_APP_TITLE: string
  readonly VITE_NODE_ENV: string
  readonly VITE_PORT: number
  readonly VITE_OPEN_SVG_LOADER: boolean
  readonly VITE_OPEN_PX2REM: boolean
  readonly VITE_OPEN_VISUALIZER: boolean
  readonly VITE_OPEN_COMPRESSION: boolean
}

interface ImportMeta {
  readonly env: ImportMetaEnv
}

interface ViteEnv extends ImportMetaEnv {}
