import { fileURLToPath, URL } from 'node:url'
import { defineConfig, loadEnv } from 'vite'

import { useBuild } from './build/build'
import { wrapperEnv } from './build/getEnv'
import { usePlugins } from './build/plugins'
import { useServer } from './build/server'

export default defineConfig(({ command, mode }) => {
  const isBuild = command === 'build'
  const root = process.cwd()
  const env = loadEnv(mode, root)
  const viteEnv = wrapperEnv(env)
  return {
    plugins: usePlugins(isBuild, viteEnv),
    server: useServer(viteEnv),
    build: useBuild(),
    resolve: {
      alias: {
        '@': fileURLToPath(new URL('./src', import.meta.url))
      }
    },
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: `
          @use "./src/styles/variables.scss" as *;
          @use "./src/styles/mixin.scss" as *;`,
          javascriptEnabled: true
        }
      }
    }
  }
})
